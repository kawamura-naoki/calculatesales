package jp.alhinc.kawamura_naoki.calculate_sales;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;



public class CalculateSales {
	//支店定義ファイル読込メソッド
	public static boolean lstFileInput(String dirName , String fileName , HashMap<String,String> namesMap ,HashMap<String,Long> salesMap, String definition,String definitionMatch){
		BufferedReader br = null;
		try {
			File branchListFile = new File(dirName,fileName);
				if(!branchListFile.exists()){
					System.out.println(definition + "定義ファイルが存在しません");
					return false;
				}

				FileReader fr = new FileReader (branchListFile);
				br = new BufferedReader (fr);
				String branchIdList;
				while((branchIdList = br.readLine()) != null) {

					//支店定義ファイル保持
					String[] branchWords = branchIdList.split(",");
					if(branchWords.length != 2 || !branchWords[0].matches(definitionMatch)) {
						System.out.println(definition + "定義ファイルのフォーマットが不正です");
						return false ;
					}

					//売上ファイル集計マップ作成
					salesMap.put(branchWords[0],0L);
					namesMap.put(branchWords[0] , branchWords[1]);
				}

		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;

		} finally {
			if(br != null){
				try{
					br.close();
				} catch(IOException e) {
					System.out.println(e);
					return false;
				}
			}
		}
		return true;
	}

	//出力メソッド
	public static boolean output(String dirName ,String fileName ,HashMap<String,String> namesMap ,HashMap<String,Long> salesMap){
		BufferedWriter bw = null;
		try{
			File file = new File(dirName,fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			for(Entry<String, String> entry : namesMap.entrySet()){
				bw.write(entry.getKey() + "," + entry.getValue() + "," + salesMap.get(entry.getKey()));
				bw.newLine();
			}

		} catch  (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(bw != null) {
				try{
					bw.close();
				}catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	public static void main(String[] args) {
		if(args.length != 1){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		//	支店定義ファイル読み込み用HashMap
		HashMap<String, String> branchDefinition = new HashMap<String, String>();
		//売上ファイル集計用HashMap
		HashMap<String,Long> aggregateMap = new HashMap<String,Long>();

		//出力メソッド引数設定
		if(lstFileInput(args[0],"branch.lst",branchDefinition,aggregateMap,"支店","^\\d{3}$") != true){
			return;
		}

		BufferedReader br = null;

		//●売上ファイル抽出
		//ファイル一覧読込
		//判定部分;
		ArrayList<File> rcdList = new ArrayList<File>();
		File dirList1 = new File(args[0]);
		File[] dirList = dirList1.listFiles();
		for(int i = 0; i < dirList.length; i++ ){
			if(dirList[i].isFile() && dirList[i].getName().matches("^\\d{8}.rcd$")) {
				rcdList.add(dirList[i]);
			}
		}
		//連番判定
		ArrayList<String> rcdBox = new ArrayList<String>();
		for(int i = 0; i < rcdList.size(); i++){
			String[] rcdNum = rcdList.get(i).getName().split("\\.");
			rcdBox.add(rcdNum[0]);
		}
		for(int i = 0; i < rcdList.size() - 1; i++){
			int rcdInt = Integer.parseInt(rcdBox.get(i));
			int rcdInt1 = Integer.parseInt(rcdBox.get(i + 1));
			if(rcdInt1 - rcdInt != 1 ){
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}
		//売上ファイルを開く;
		for(int i = 0; i < rcdList.size(); i++){
			ArrayList<String> rcdWords = new ArrayList<String>();
			try {
				File rcdContentsFile = rcdList.get(i);
				FileReader fr = new FileReader (rcdContentsFile);
				br = new BufferedReader (fr);
				String rcdContents;
				while((rcdContents = br.readLine()) != null) {
					rcdWords.add(rcdContents);
				}
				//集計部分
				if(rcdWords.size() != 2){
					System.out.println(rcdList.get(i) + "のフォーマットが不正です");
					return;
				}
				if(!rcdWords.get(1).matches("^\\d+$")){
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				long rcdLong = Long.valueOf(rcdWords.get(1)).longValue();
				if(!aggregateMap.containsKey(rcdWords.get(0))){
					System.out.println(rcdList.get(i) + "の支店コードが不正です");
					return;
				}
				if(10 < String.valueOf(rcdLong + aggregateMap.get(rcdWords.get(0))).length()){
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				aggregateMap.put(rcdWords.get(0), rcdLong + aggregateMap.get(rcdWords.get(0)));
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;

			}finally{
				if(br != null){
					try{
						br.close();
					}catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}
		//ファイル出力引数設定
		if(output(args[0],"branch.out",branchDefinition,aggregateMap) != true){
			return;
		}
	}
}

